﻿using System.Collections.Generic;
using System.Linq;

namespace DemoNancy.Model
{
    public class Portfolio
    {
        private List<IHolding> _holdings;
        private readonly HoldingValuationCalculator _valuationCalculator;

        public int PortfolioId { get; set; }

        public List<IHolding> Holdings { get { return _holdings; } }

        public HoldingValuationCalculator ValuationCalculator { get { return _valuationCalculator; } }

        public decimal InitialInvestment { get { return _holdings.Sum(holding => holding.InitialInvestmentValue()); } }

        public decimal CurrentInvestmentValue { get { return _holdings.Sum(holding => holding.CurrentInvestmentValue(_valuationCalculator)); } }

        public Portfolio(HoldingValuationCalculator valuationCalculator)
        {
            _holdings = new List<IHolding>();
            _holdings.Add(new EquityHolding());
            _holdings.Add(new MutualFundHolding());
            _holdings.Add(new DerivativeHolding());
            _valuationCalculator = valuationCalculator;
        }

        public void AddTradeToHolding(ITrade trade)
        {
            var holding = _holdings.SingleOrDefault(asset => asset.AssetClass == trade.ProductName);
            if (holding != null)
                holding.AddTrade(trade);
        }
    }
}