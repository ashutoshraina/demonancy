﻿namespace DemoNancy.Model
{
    public class StockStat
    {
        private readonly decimal? _trailingPE;
        private readonly decimal? _priceBook;
        private readonly decimal? _yearHigh;
        private readonly decimal? _yearLow;
        private readonly string _marketCapital;

        public decimal? TrailingPE { get { return _trailingPE; } }

        public decimal? PriceBook { get { return _priceBook; } }

        public decimal? YearHigh { get { return _yearHigh; } }

        public decimal? YearLow { get { return _yearLow; } }

        public string MarketCapital { get { return _marketCapital; } }

        public StockStat(decimal? trailingPE, decimal? priceBook, decimal? yearHigh, decimal? yearLow, string marketCapitlal)
        {
            _trailingPE = trailingPE;
            _priceBook = priceBook;
            _yearHigh = yearHigh;
            _yearLow = yearLow;
            _marketCapital = marketCapitlal;
        }
    }
}