﻿namespace DemoNancy.Model
{
    public class DerivativeTrade : ITrade
    {
        private readonly TradeType _tradeType = TradeType.Buy;

        public string ProductName { get { return AssetClasses.Derivative; } }

        public TradeType TradeType { get { return _tradeType; } }
    }

    public class DerivativeHolding : IHolding
    {
        public string AssetClass { get { return AssetClasses.Derivative; } }

        public void AddTrade(ITrade trade)
        {
        }

        public decimal CurrentInvestmentValue(HoldingValuationCalculator calculator)
        {
            return 250000;
        }

        public decimal InitialInvestmentValue()
        {
            return 150000;
        }
    }
}