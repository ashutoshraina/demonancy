﻿namespace DemoNancy.Model.Yahoo
{
    static class YahooFinanceApiUrls
    {
        internal static string BaseServiceUrl = "http://finance.yahoo.com/webservice/v1";

        internal static string BaseYQLUrl = "http://query.yahooapis.com/v1/public";
    }
}