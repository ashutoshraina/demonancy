﻿using Refit;
using System.Threading.Tasks;

namespace DemoNancy.Model.Yahoo
{
    interface IYahooFinanceApi
    {
        [Get("/symbols/{symbol}/quote?format={format}")]
        Task<string> GetQuote(string symbol, string format = "json");

        [Get("/yql?env={env}&q={query}&format={format}")]
        Task<string> GetStockInfo(string query, string format = "json", string env = "store://datatables.org/alltableswithkeys");
    }
}