﻿namespace DemoNancy.Model
{
    public interface ITrade
    {
        string ProductName { get; }

        TradeType TradeType { get; }
    }

    public enum TradeType
    {
        Buy,
        Sell
    }
}
