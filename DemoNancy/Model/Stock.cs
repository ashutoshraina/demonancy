﻿using System;

namespace DemoNancy.Model
{
    public class Stock
    {
        private readonly string _symbol;
        private readonly string _name;
        private readonly decimal _currentPrice;
        private readonly DateTime _timeStamp;

        public string Symbol { get { return _symbol; } }

        public string Name { get { return _name; } }

        public decimal CurrentPrice { get { return _currentPrice; } }

        public DateTime TimeStamp { get { return _timeStamp; } }

        public IntraDayStat IntraDay { get; set; }

        public StockStat StockStats { get; set; }
        public Stock(string symbol, string name, decimal currentPrice, DateTime timeStamp)
        {
            _symbol = symbol;
            _name = name;
            _currentPrice = currentPrice;
            _timeStamp = timeStamp;

        }
    }
}