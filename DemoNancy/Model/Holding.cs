﻿namespace DemoNancy.Model
{
    public static class AssetClasses
    {
        public static string Equity = "Equity";
        public static string MutualFund = "Mutual Fund";
        public static string Derivative = "Derivatives";
    }

    public interface IHolding
    {
        string AssetClass { get; }

        decimal InitialInvestmentValue();

        decimal CurrentInvestmentValue(HoldingValuationCalculator calculator);

        void AddTrade(ITrade trade);
    }
}