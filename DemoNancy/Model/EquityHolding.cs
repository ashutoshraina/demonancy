﻿using System.Collections.Generic;
using System.Linq;

namespace DemoNancy.Model
{
    public class EquityHolding : IHolding
    {
        private List<StockTrade> _trades;
        
        public List<StockTrade> StockTrades { get { return _trades; } }
        public EquityHolding()
        {
            _trades = new List<StockTrade>();
        }

        public decimal CurrentInvestmentValue(HoldingValuationCalculator calculator)
        {
            return calculator.EquityInvestmentValue(this);
        }

        public decimal InitialInvestmentValue()
        {
            return _trades.Sum(trade => trade.GetTradedValue());
        }

        public string AssetClass { get { return AssetClasses.Equity; } }
        public void AddTrade(ITrade trade)
        {
            var stockTrade = (StockTrade)trade;
            switch (trade.TradeType)
            {
                case TradeType.Buy:
                    _trades.Add(stockTrade);
                    break;
                case TradeType.Sell:
                    break;
            }
        }        
    }
}