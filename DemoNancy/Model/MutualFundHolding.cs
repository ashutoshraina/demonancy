﻿namespace DemoNancy.Model
{
    public class MutualFundTrade : ITrade
    {
        private readonly TradeType _tradeType = TradeType.Buy;

        public string ProductName { get { return AssetClasses.MutualFund; } }

        public TradeType TradeType { get { return _tradeType; } }
    }

    public class MutualFundHolding : IHolding
    {
        public string AssetClass { get { return AssetClasses.MutualFund; } }

        public void AddTrade(ITrade trade)
        {
        }

        public decimal CurrentInvestmentValue(HoldingValuationCalculator calculator)
        {
            return 78000;
        }

        public decimal InitialInvestmentValue()
        {
            return 100000;
        }
    }
}