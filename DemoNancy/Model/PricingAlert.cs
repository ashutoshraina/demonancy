﻿namespace DemoNancy.Model
{
    public class PricingAlert
    {
        private readonly decimal _triggerPrice;
        private readonly string _symbol;

        public string Symbol { get { return _symbol; } }
        public decimal TriggerPrice { get { return _triggerPrice; } }

        public PricingAlert(string symbol, decimal triggerPrice)
        {
            _symbol = symbol;
            _triggerPrice = triggerPrice;
        }

        public bool ShouldAlertBeTriggered(decimal currentStockPrice)
        {
            return currentStockPrice >= _triggerPrice;
        }
    }
}