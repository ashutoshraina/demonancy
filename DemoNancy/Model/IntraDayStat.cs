﻿namespace DemoNancy.Model
{
    public class IntraDayStat
    {
        public decimal? AskPrice { get; set; }

        public decimal? BidPrice { get; set; }

        public decimal? Change { get; set; }

        public string PercentChange { get; set; }

        public decimal? DaysHigh { get; set; }

        public decimal? DaysLow { get; set; }

        public decimal? Open { get; set; }

        public decimal? Volume { get; set; }

        public decimal? PreviousClose { get; set; }

        public string DividendYield { get; set; }
    }
}