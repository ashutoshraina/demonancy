﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoNancy.Model
{
    public class StockTrade : ITrade
    {
        private readonly Stock _stock;
        private readonly decimal _askingPrice;
        private readonly int _quantity;
        private readonly DateTime _timeStamp;
        private readonly TradeType _tradeType;

        public Stock Stock { get { return _stock; } }

        public decimal AskingPrice { get { return _askingPrice; } }

        public int Quantity { get { return _quantity; } }

        public DateTime TradedOn { get { return _timeStamp; } }

        public string ProductName { get { return AssetClasses.Equity; } }

        public TradeType TradeType { get { return _tradeType; } }

        public StockTrade(Stock stock, decimal buyingPrice, int quantity, TradeType tradeType)
        {
            _stock = stock;
            _askingPrice = buyingPrice;
            _quantity = quantity;
            _timeStamp = DateTime.Now;
            _tradeType = tradeType;
        }

        public decimal GetTradedValue()
        {
            return _askingPrice * _quantity;
        }

        public decimal GetCurrentValue(decimal currentPrice)
        {
            return currentPrice * _quantity;
        }
    }
}