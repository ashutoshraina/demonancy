﻿using System.Collections.Generic;

namespace DemoNancy.Model
{
    public class User
    {
        private readonly string _userName;
        private readonly Portfolio _portfolio;

        private readonly List<PricingAlert> _alerts = new List<PricingAlert>();

        public string UserName { get { return _userName; } }

        public Portfolio Portfolio { get { return _portfolio; } }

        public IEnumerable<PricingAlert> RegisteredAlerts { get { return _alerts.ToArray(); } }

        public User(string userName, Portfolio portfolio)
        {
            _userName = userName;
            _portfolio = portfolio;
        }

        public void RegisterAlert(PricingAlert alert)
        {
            _alerts.Add(alert);
        }

        public void RemoveAlert(PricingAlert alert)
        {
            _alerts.Remove(alert);
        }
    }
}