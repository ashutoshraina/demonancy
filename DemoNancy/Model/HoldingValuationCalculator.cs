﻿using System.Collections.Generic;
using System.Linq;

namespace DemoNancy.Model
{
    public class HoldingValuationCalculator
    {
        private List<Stock> _stocks;
        public decimal EquityInvestmentValue(EquityHolding equity) => equity.StockTrades.Sum(trade => trade.GetCurrentValue(_stocks.SingleOrDefault(stock => stock.Symbol == trade.Stock.Symbol).CurrentPrice));

        public HoldingValuationCalculator(List<Stock> stocks)
        {
            _stocks = stocks;
        }
    }
}