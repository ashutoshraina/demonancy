﻿using DemoNancy.Model.Yahoo;
using Nancy;
using Refit;

namespace DemoNancy.Modules
{
    public class HomeModule : NancyModule
    {
        public HomeModule()
        {
            Get["/"] = parameter => "Hello World 2";

            Get["/test/realtime", runAsync: true] = async (parameter, cancellationToken) =>
            {
                var yahooFinanceApi = RestService.For<IYahooFinanceApi>(YahooFinanceApiUrls.BaseYQLUrl);
                var queryString = "select * from geo.places where text='sunnyvale'";
                var response = await yahooFinanceApi.GetStockInfo(queryString);
                return Response.AsText(response);
            };
        }
    }
}