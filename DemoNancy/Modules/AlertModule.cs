﻿using DemoNancy.Factory;
using DemoNancy.Model;
using Nancy;
using System.Linq;

namespace DemoNancy.Modules
{
    public class AlertModule : NancyModule
    {
        public AlertModule() : base("/alert")
        {
            Post["/{userName}/priceAlert"] = parameter =>
            {
                var userName = (string)parameter.userName;
                var symbol = ((string)Request.Form["symbol"]).ToUpper();
                var price = (decimal)Request.Form["price"];
                Application.GetInstance().Users.First(user => user.UserName == userName).RegisterAlert(new PricingAlert(symbol, price));
                return Response.AsJson(new { status = "Pricing Alert Registered" });
            };

            Get["/{userName}/priceAlert"] = parameter =>
            {
                var userName = (string)parameter.userName;
                var symbol = ((string)Request.Query["symbol"]).ToUpper();
                var user = Application.GetInstance().Users.First(u => u.UserName == userName);
                var stock = StockFactory.GetStock(symbol);
                var alerts = user.RegisteredAlerts.ToList();
                if (alerts.Count == 0)
                {
                    return Response.AsJson(new { status = "No Pricing Alerts Registered" });
                }
                var alert = alerts.First(a => a.Symbol == symbol);
                if (alert.ShouldAlertBeTriggered(stock.CurrentPrice))
                {
                    user.RemoveAlert(alert);
                    return Response.AsJson(new { status = "Pricing Alert Triggered" });
                }

                return Response.AsJson(new { status = "Pricing Alert Not Triggered" });
            };

            Get["/{userName}/priceAlert/list"] = parameter =>
            {
                var userName = (string)parameter.userName;
                var user = Application.GetInstance().Users.First(u => u.UserName == userName);
                var alerts = user.RegisteredAlerts;
                return Response.AsJson(alerts);
            };
        }
    }
}