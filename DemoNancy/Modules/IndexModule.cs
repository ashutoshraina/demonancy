﻿using DemoNancy.Model.Yahoo;
using Nancy;
using Newtonsoft.Json.Linq;
using Refit;

namespace DemoNancy.Modules
{
    public class IndexModule : NancyModule
    {
        public IndexModule() : base("/index")
        {
            Get["/nse", runAsync: true] = async (parameter, cancellationToken) =>
            {
                var yahooFinanceApi = RestService.For<IYahooFinanceApi>(YahooFinanceApiUrls.BaseYQLUrl);
                var symbol = "^NSEI";
                var indexQuery = "select * from yahoo.finance.quote where symbol = '" + symbol + "'";
                var stockFields = JObject.Parse(await yahooFinanceApi.GetStockInfo(indexQuery)).SelectToken("$.query.results.quote");

                var change = stockFields.SelectToken("$.Change").ToString();
                var currentPrice = stockFields.SelectToken("$.LastTradePriceOnly").ToString();
                var daysLow = stockFields.SelectToken("$.DaysLow").ToString();
                var daysHigh = stockFields.SelectToken("$.DaysHigh").ToString();
                return Response.AsJson(new { symbol, currentPrice, change, daysLow, daysHigh });
            };

            Get["/bse", runAsync: true] = async (parameter, cancellationToken) =>
            {
                var yahooFinanceApi = RestService.For<IYahooFinanceApi>(YahooFinanceApiUrls.BaseYQLUrl);
                var symbol = "^BSESN";
                var indexQuery = "select * from  yahoo.finance.quote where symbol = '" + symbol + "'";
                var stockFields = JObject.Parse(await yahooFinanceApi.GetStockInfo(indexQuery)).SelectToken("$.query.results.quote");

                var change = stockFields.SelectToken("$.Change").ToString();
                var currentPrice = stockFields.SelectToken("$.LastTradePriceOnly").ToString();
                var daysLow = stockFields.SelectToken("$.DaysLow").ToString();
                var daysHigh = stockFields.SelectToken("$.DaysHigh").ToString();
                return Response.AsJson(new { symbol, currentPrice, change, daysLow, daysHigh });
            };
        }
    }
}