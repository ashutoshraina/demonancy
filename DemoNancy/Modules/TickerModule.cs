﻿using DemoNancy.Factory;
using DemoNancy.Model.Yahoo;
using Nancy;
using Newtonsoft.Json.Linq;
using Refit;
using System.Collections.Generic;
using System.Linq;

namespace DemoNancy.Modules
{
    public class TickerModule : NancyModule
    {
        private IYahooFinanceApi _financeApi = RestService.For<IYahooFinanceApi>(YahooFinanceApiUrls.BaseYQLUrl);

        public TickerModule() : base("/ticker")
        {
            Get["/{symbol}", runAsync: true] = async (parameter, cancellationToken) =>
            {
                var startDate = (string)Request.Query["startDate"];
                var endDate = (string)Request.Query["endDate"];
                var symbol = ((string)parameter.symbol).ToUpper();
                var yahooFinanceApi = RestService.For<IYahooFinanceApi>(YahooFinanceApiUrls.BaseYQLUrl);
                var queryString = "select * from yahoo.finance.historicaldata where symbol = + '" + symbol + "' and startDate = '" + startDate + "' and endDate = '" + endDate + "'";
                var stockHistory = JObject.Parse(await yahooFinanceApi.GetStockInfo(queryString)).SelectToken("$.query.results.quote").ToString();

                return Response.AsText(stockHistory);
            };

            Get["/topFive", runAsync: true] = async (parameter, cancellationToken) =>
            {
                var joinedSymbols = string.Join(",", StockSymbol.Symbols.Union(StockSymbol.ExtendedSymbols.Take(15)));
                var intraDayQuery = "select Symbol, Change, PercentChange, DaysHigh, DaysLow, Volume from yahoo.finance.quotes where symbol in ('" + joinedSymbols + "')";
                var intraDayData = JObject.Parse(await _financeApi.GetStockInfo(intraDayQuery).ConfigureAwait(false)).SelectToken("$.query.results.quote");
                var result = intraDayData.Select(data => new
                {
                    symbol = data["Symbol"].ToString(),
                    change = data["Change"].ToString(),
                    percentChange = decimal.Parse(data["PercentChange"].ToString().Replace("%", "")),
                    daysHigh = data["DaysHigh"],
                    daysLow = data["DaysLow"],
                    volume = data["Volume"]
                }).OrderBy(item => item.percentChange).Reverse().Take(5).ToList();
                return Response.AsJson(result);
            };

            Get["/bottomFive", runAsync: true] = async (parameter, cancellationToken) =>
            {
                var joinedSymbols = string.Join(",", StockSymbol.Symbols.Union(StockSymbol.ExtendedSymbols.Take(15)));
                var intraDayQuery = "select Symbol, Change, PercentChange, DaysHigh, DaysLow, Volume from yahoo.finance.quotes where symbol in ('" + joinedSymbols + "')";
                var intraDayData = JObject.Parse(await _financeApi.GetStockInfo(intraDayQuery).ConfigureAwait(false)).SelectToken("$.query.results.quote");
                var result = intraDayData.Select(data => new
                {
                    symbol = data["Symbol"].ToString(),
                    change = data["Change"].ToString(),
                    percentChange = decimal.Parse(data["PercentChange"].ToString().Replace("%", "")),
                    daysHigh = data["DaysHigh"],
                    daysLow = data["DaysLow"],
                    volume = data["Volume"]
                }).OrderBy(item => item.percentChange).Take(5).ToList();
                return Response.AsJson(result);
            };
        }
    }
}