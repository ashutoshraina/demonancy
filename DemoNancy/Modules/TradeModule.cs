﻿using DemoNancy.Factory;
using DemoNancy.Model;
using Nancy;
using System;
using System.Linq;


namespace DemoNancy.Modules
{
    public class TradeModule : NancyModule
    {
        public TradeModule() : base("/trade")
        {
            Get["/{userName}/buy/"] = parameter =>
            {
                var userName = (string)parameter.userName;
                var symbol = (string)Request.Query["symbol"];
                var quantity = (Int32)Request.Query["quantity"];
                var stock = StockFactory.GetStock(symbol);
                var trade = new StockTrade(stock,stock.CurrentPrice,quantity, TradeType.Buy);
                var portfolio = Application.GetInstance().Users.First(user => user.UserName == userName).Portfolio;
                portfolio.AddTradeToHolding(trade);
                return Response.AsJson(new { status = "Order Completed"});
            };
            Get["/{userName}/sell/"] = parameter =>
            {
                var userName = (string)parameter.userName;
                var symbol = (string)Request.Query["symbol"];
                var quantity = (Int32)Request.Query["quantity"];
                var stock = StockFactory.GetStock(symbol);
                var trade = new StockTrade(stock, stock.CurrentPrice, quantity, TradeType.Sell);
                var portfolio = Application.GetInstance().Users.First(user => user.UserName == userName).Portfolio;
                portfolio.AddTradeToHolding(trade);
                return Response.AsJson(new { status = "Order Completed" });
            };
        }
    }
}