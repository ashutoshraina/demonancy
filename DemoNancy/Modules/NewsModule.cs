﻿using DemoNancy.Model.Yahoo;
using Nancy;
using Newtonsoft.Json.Linq;
using Refit;

namespace DemoNancy.Modules
{
    public class NewsModule : NancyModule
    {
        public NewsModule() : base("/news")
        {
            Get["/{symbol}", runAsync: true] = async (parameter, cancellationToken) =>
            {
                var queryString = "select * from rss where url = 'http://finance.yahoo.com/rss/headline?s=" + (string)parameter.symbol + "'";
                var yahooFinanceApi = RestService.For<IYahooFinanceApi>(YahooFinanceApiUrls.BaseYQLUrl);
                var news = JObject.Parse(await yahooFinanceApi.GetStockInfo(queryString)).SelectToken("$.query.results.item").ToString();
                return Response.AsText(news);
            };

            Get["/", runAsync: true] = async (parameter, cancellationToken) =>
            {
                var queryString = "select * from rss where url = 'http://finance.yahoo.com/rss/topfinstories'";
                var yahooFinanceApi = RestService.For<IYahooFinanceApi>(YahooFinanceApiUrls.BaseYQLUrl);
                var temp = await yahooFinanceApi.GetStockInfo(queryString);
                var news = JObject.Parse(await yahooFinanceApi.GetStockInfo(queryString)).SelectToken("$.query.results.item").ToString();
                return Response.AsText(news);
            };
        }
    }
}