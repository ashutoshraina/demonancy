﻿using DemoNancy.Factory;
using DemoNancy.Model;
using Nancy;
using System.Linq;

namespace DemoNancy.Modules
{
    public class PortfolioModule : NancyModule
    {
        private Application _application;

        public PortfolioModule() : base("/portfolio")
        {
            _application = Application.GetInstance();

            Get["/{userName}"] = parameter =>
              {
                  var user = _application.Users.FirstOrDefault(u => u.UserName == (string)parameter.userName);

                  return Response.AsJson(new
                  {
                      baseInvestment = user.Portfolio.InitialInvestment,
                      currentValuation = user.Portfolio.CurrentInvestmentValue,
                      holdings = user.Portfolio.Holdings.Select(holding => new
                      {
                          holding.AssetClass,
                          currentValuation = holding.CurrentInvestmentValue(user.Portfolio.ValuationCalculator),
                          initialInvestment = holding.InitialInvestmentValue()
                      })
                  });
              };

            Get["/{userName}/equity"] = parameter =>
            {
                var _currentStockPrices = StockFactory.GetStocks(StockSymbol.Symbols);
                var user = _application.Users.FirstOrDefault(u => u.UserName == (string)parameter.userName);
                var equityHolding = (EquityHolding)user.Portfolio.Holdings.FirstOrDefault(holding => holding.AssetClass == AssetClasses.Equity);

                var netAssets = equityHolding.StockTrades.Select(trade => new
                {
                    symbol = trade.Stock.Symbol,
                    name = trade.Stock.Name,
                    quantity = trade.Quantity,
                    buyingPrice = trade.AskingPrice,
                    currentTradeValue = _currentStockPrices.First(stock => stock.Symbol == trade.Stock.Symbol).CurrentPrice * trade.Quantity
                }).ToList();
                var result = from stock in netAssets
                             group stock by stock.symbol into g
                             select new
                             {
                                 symbol = g.Key,
                                 name = g.First().name,
                                 quantity = g.Sum(x => x.quantity),
                                 buyingPrice = g.Average(x => x.buyingPrice),
                                 netAssetValue = g.Sum(x => x.currentTradeValue)
                             };
                return Response.AsJson(result);
            };

            Get["/{userName}/equity/{symbol}"] = parameter =>
            {
                var symbol = ((string)parameter.symbol).ToUpper();
                var currentStockPrice = StockFactory.GetStock(symbol).CurrentPrice;
                var user = _application.Users.FirstOrDefault(u => u.UserName == (string)parameter.userName);
                var equityHolding = (EquityHolding)user.Portfolio.Holdings.FirstOrDefault(holding => holding.AssetClass == AssetClasses.Equity);
                var trades = equityHolding.StockTrades
                                        .Where(trade => trade.Stock.Symbol == symbol)
                                        .Select(trade => new
                                        {
                                            symbol = trade.Stock.Symbol,
                                            name = trade.Stock.Name,
                                            currentPrice = currentStockPrice,
                                            quantity = trade.Quantity,
                                            buyingPrice = trade.AskingPrice,
                                            tradedOn = trade.TradedOn.ToLocalTime()
                                        }).ToList();

                return Response.AsJson(trades);
            };
        }
    }
}