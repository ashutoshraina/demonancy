﻿using DemoNancy.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace DemoNancy.Model
{
    public class Application
    {
        public List<User> Users;
        private static Application _instance;
        private Application()
        {
            
        }

        public static Application GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Application();
                _instance.Users = UserFactory.GetUsers();
            }

            return _instance;
        }
        
    }
}