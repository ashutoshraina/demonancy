﻿using DemoNancy.Factory;
using DemoNancy.Model;
using DemoNancy.Model.Yahoo;
using Nancy;
using Newtonsoft.Json.Linq;
using Refit;
using System;
using System.Threading.Tasks;

namespace DemoNancy.Modules
{
    public static class Conversions
    {
        public static IntraDayStat ToIntraDayStat(this JToken intraDayData)
        {
            var askPrice = Convert.ToDecimal((string)intraDayData.SelectToken("$.Ask"));
            var bidPrice = Convert.ToDecimal((string)intraDayData.SelectToken("$.Bid"));
            var change = Convert.ToDecimal((string)intraDayData.SelectToken("$.Change"));
            var percentChange = intraDayData.SelectToken("$.PercentChange").ToString();
            var daysLow = Convert.ToDecimal((string)intraDayData.SelectToken("$.DaysLow"));
            var daysHigh = Convert.ToDecimal((string)intraDayData.SelectToken("$.DaysHigh"));
            var open = Convert.ToDecimal((string)intraDayData.SelectToken("$.Open"));
            var previousClose = Convert.ToDecimal((string)intraDayData.SelectToken("$.PreviousClose"));
            var volume = Convert.ToDecimal((string)intraDayData.SelectToken("$.Volume"));
            var dividendYield = intraDayData.SelectToken("$.DividendYield").ToString();

            var intraDayStat = new IntraDayStat
            {
                AskPrice = askPrice,
                BidPrice = bidPrice,
                Change = change,
                PercentChange = percentChange,
                DaysLow = daysLow,
                DaysHigh = daysHigh,
                Open = open,
                PreviousClose = previousClose,
                DividendYield = dividendYield,
                Volume = volume
            };

            return intraDayStat;
        }

        public static StockStat ToStockStat(this JToken statsData)
        {
            if (statsData == null)
            {
                return new StockStat(0, 0, 0, 0, null);
            }
            var trailingPE = Convert.ToDecimal(GetValue(statsData, "$.TrailingPE.content", "$.TrailingPE"));
            var marketCapital = GetValue(statsData, "$.MarketCap.content", "$.MarketCap");
            var priceBook = Convert.ToDecimal(GetValue(statsData, "$.PriceBook.content", "$.TrailingPE"));
            var yearHigh = Convert.ToDecimal(GetValue(statsData, "$.p_52_WeekHigh.content", "$.p_52_WeekHigh"));
            var yearLow = Convert.ToDecimal(GetValue(statsData, "$.p_52_WeekLow.content", "$.p_52_WeekLow"));
            var stockStats = new StockStat(trailingPE, priceBook, yearHigh, yearLow, marketCapital);
            return stockStats;
        }

        private static string GetValue(JToken token, string firstPath, string alternateJsonPath)
        {
            return token.SelectToken(firstPath) == null ? token.SelectToken(alternateJsonPath).ToString() : token.SelectToken(firstPath).ToString();
        }
    }

    public class EquityModule : NancyModule
    {
        private IYahooFinanceApi yahooFinanceApi = RestService.For<IYahooFinanceApi>(YahooFinanceApiUrls.BaseYQLUrl);

        public EquityModule() : base("/equity")
        {
            Get["/{symbol}", runAsync: true] = async (parameter, cancellationToken) =>
            {
                var symbol = ((string)parameter.symbol).ToUpper();
                var stock = StockFactory.GetStock(symbol);
                var statsData = await GetStatsData(symbol);
                stock.StockStats = statsData.ToStockStat();
                var intraDayQuery = "select Ask, Bid, Change, PercentChange, DaysLow, DaysHigh, Open, PreviousClose,Volume, DividendYield from yahoo.finance.quotes where symbol in ('" + symbol + "')";
                var intraDayData = JObject.Parse(await yahooFinanceApi.GetStockInfo(intraDayQuery).ConfigureAwait(false)).SelectToken("$.query.results.quote");
                var intraDayStat = intraDayData.ToIntraDayStat();
                stock.IntraDay = intraDayStat;
                return Response.AsJson(stock);
            };
        }

        private async Task<JToken> GetStatsData(string symbol)
        {
            var statsQuery = "select MarketCap, TrailingPE, PriceBook, p_52_WeekHigh, p_52_WeekLow  from  yahoo.finance.keystats where symbol in ('" + symbol + "')";
            return JObject.Parse(await yahooFinanceApi.GetStockInfo(statsQuery).ConfigureAwait(false)).SelectToken("$.query.results.stats");
        }
    }
}