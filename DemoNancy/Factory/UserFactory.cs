﻿using DemoNancy.Model;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoNancy.Factory
{
    public static class UserFactory
    {
        public static List<User> GetUsers()
        {
            var portfolios = PortfolioFactory.GetPortfolio();
            var venkat = new User("venkat", portfolios.First());
            var arjun = new User("arjun", portfolios.Last());
            return new List<User> { venkat, arjun };
        }
    }
}