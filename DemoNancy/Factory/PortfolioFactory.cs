﻿using DemoNancy.Model;
using System;
using System.Collections.Generic;

namespace DemoNancy.Factory
{
    public static class PortfolioFactory
    {
        public static List<Portfolio> GetPortfolio()
        {
            var stocks = StockFactory.GetStocks(StockSymbol.Symbols);
            var portfolio1 = new Portfolio(new HoldingValuationCalculator(stocks))
            {
                PortfolioId = 101
            };
            var portfolio2 = new Portfolio(new HoldingValuationCalculator(stocks))
            {
                PortfolioId = 102
            };
            var currentPriceRandomiser = new Random();
            var quantityRandomiser = new Random();
            var tradeRandomiser = new Random();

            foreach (var stock in stocks)
            {
                int numberOfTrades = tradeRandomiser.Next(1, 5);
                for (int i = 0; i < numberOfTrades; i++)
                {
                    portfolio1.AddTradeToHolding(new StockTrade(stock, stock.CurrentPrice + currentPriceRandomiser.Next(-10, 5), quantityRandomiser.Next(500, 1000), TradeType.Buy));
                    portfolio2.AddTradeToHolding(new StockTrade(stock, stock.CurrentPrice + currentPriceRandomiser.Next(-5, 10), quantityRandomiser.Next(500, 1000), TradeType.Buy));
                }
            }

            portfolio1.AddTradeToHolding(new MutualFundTrade());
            portfolio1.AddTradeToHolding(new DerivativeTrade());
            return new List<Portfolio> { portfolio1, portfolio2 };
        }
    }
}