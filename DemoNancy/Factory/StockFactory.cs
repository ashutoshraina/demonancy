﻿using DemoNancy.Model;
using DemoNancy.Model.Yahoo;
using Newtonsoft.Json.Linq;
using Refit;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace DemoNancy.Factory
{
    public static class StockSymbol
    {
        public static List<string> Symbols = new List<string> { "HINDALCO.NS", "SBIN.NS", "COALINDIA.NS", "INFY.NS", "TCS.NS", "KOTAKBANK.NS" };

        public static List<string> ExtendedSymbols = new List<string> { "ARVIND.NS", "AXISBANK.NS", "BANKINDIA.NS","BHEL.NS","BIOCON.NS","BOSCHLTD.NS","BPCL.NS","CAIRN.NS","CANBK.NS", "CIPLA.NS", "DABUR.NS","DISHTV.NS","DIVISLAB.NS","DLF.NS","DRREDDY.NS","EICHERMOT.NS", "ENGINERSIN.NS",
"EXIDEIND.NS","FEDERALBNK.NS","GAIL.NS","GLENMARK.NS","GMRINFRA.NS","GODREJIND.NS","GRASIM.NS","HAVELLS.NS","HCLTECH.NS","HDFC.NS","HDFCBANK.NS","HDIL.NS","HEROMOTOCO.NS","HEXAWARE.NS","HINDPETRO.NS","HINDUNILVR.NS","HINDZINC.NS",
"IBREALEST.NS","IBULHSGFIN.NS","ICICIBANK.NS", "IDBI.NS", "IDEA.NS", "IDFC.NS", "IFCI.NS", "IGL.NS", "INDIACEM.NS", "INDUSINDBK.NS", "IOB.NS","IOC.NS", "IRB.NS", "ITC.NS", "JINDALSTEL.NS", "JISLJALEQS.NS","JPASSOCIAT.NS", "JPPOWER.NS",
"JSWENERGY.NS", "JSWSTEEL.NS", "JUBLFOOD.NS", "JUSTDIAL.NS", "KOTAKBANK.NS", "KTKBANK.NS", "L&TFH.NS", "LICHSGFIN.NS", "LT.NS", "LUPIN.NS", "M&M.NS", "M&MFIN.NS", "MARUTI.NS", "MCLEODRUSS.NS", "MINDTREE.NS", "MOTHERSUMI.NS", "MRF.NS", "NHPC.NS", "NMDC.NS", "NTPC.NS",
"OFSS.NS", "ONGC.NS", "ORIENTBANK.NS", "PETRONET.NS", "PFC.NS","PNB.NS","POWERGRID.NS","PTC.NS","RANBAXY.NS","RCOM.NS","RECLTD.NS","RELCAPITAL.NS","RELIANCE.NS","RELINFRA.NS","RPOWER.NS","SAIL.NS","SBIN.NS","SIEMENS.NS",
"SKSMICRO.NS","STAR.NS","SUNPHARMA.NS","SUNTV.NS","SYNDIBANK.NS","TATACHEM.NS","TATACOMM.NS","TATAGLOBAL.NS","TATAMOTORS.NS","TATAMTRDVR.NS","TATAPOWER.NS","TATASTEEL.NS","TECHM.NS","TITAN.NS","TVSMOTOR.NS","UBL.NS","UCOBANK.NS","ULTRACEMCO.NS","UNIONBANK.NS","UNITECH.NS","UPL.NS","VOLTAS.NS","WIPRO.NS","WOCKPHARMA.NS","YESBANK.NS","ZEEL.NS" };
    }

    public static class StockFactory
    {
        private static List<Stock> _stocks = new List<Stock>();
        private static DateTime _lastUpdated = DateTime.Now;

        public static List<Stock> GetStocks(List<string> symbols)
        {
            if (_stocks.Count == 0)
            {
                UpdateStockCache(symbols);
            }
            else
            {
                Task.Factory.StartNew(() =>
                {
                    UpdateStockCache(symbols);
                })
                .ContinueWith(t => { if (t.IsCanceled || t.IsFaulted) Trace.TraceError("Cache update operation errored out"); })
                .ConfigureAwait(false);
            }

            return _stocks;
        }

        public static Stock GetStock(string symbol)
        {
            var stocks = GetStocks(StockSymbol.Symbols);
            return stocks.First(stock => stock.Symbol == symbol);
        }

        private static void UpdateStockCache(List<string> symbols)
        {
            var joinedSymbols = string.Join(",", symbols);
            var taskResult = Task.Factory.StartNew(() => GetQuote(joinedSymbols)).Result.Result;
            var stockFields = JObject.Parse(taskResult);
            var freshStocks = new List<Stock>();

            for (int i = 0; i < symbols.Count; i++)
            {
                freshStocks.Add(ConvertToStock(stockFields, i));
            }
            _stocks = freshStocks;
            _lastUpdated = DateTime.Now;
            Trace.TraceInformation("Updating the cache at " + DateTime.Now);
        }

        private static Stock ConvertToStock(JToken stockFields, int index)
        {
            var fields = stockFields.SelectToken("$.list.resources[" + index + "].resource.fields");
            var symbol = fields.SelectToken("$.symbol").ToString();
            var name = fields.SelectToken("$.name").ToString();
            var currentPrice = decimal.Parse(fields.SelectToken("$.price").ToString());
            var timeStamp = DateTime.Parse(fields.SelectToken("$.utctime").ToString());
            return new Stock(symbol, name, currentPrice, timeStamp);
        }

        public async static Task<string> GetQuote(string symbols)
        {
            var yahooStockQuoteApi = RestService.For<IYahooFinanceApi>(YahooFinanceApiUrls.BaseServiceUrl);
            var taskResult = (await yahooStockQuoteApi.GetQuote(symbols).ConfigureAwait(false));
            return taskResult;
        }

        public static List<Stock> GetExtendedStocks(List<string> symbols)
        {
            var joinedSymbols = string.Join(",", symbols);
            var taskResult = Task.Factory.StartNew(() => GetQuote(joinedSymbols)).Result.Result;
            var stockFields = JObject.Parse(taskResult);
            var freshStocks = new List<Stock>();

            for (int i = 0; i < symbols.Count; i++)
            {
                try
                {
                    freshStocks.Add(ConvertToStock(stockFields, i));
                }
                catch (Exception)
                {
                    Trace.TraceError("Error Converting Stock : " + symbols[i]);
                }
            }

            return freshStocks;
        }
    }
}